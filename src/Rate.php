<?php

declare(strict_types=1);

namespace App;

final class Rate
{
    public int $rate;
    public int $worker;

    public function __construct(int $rate, int $worker)
    {
        $this->rate = $rate;
        $this->worker = $worker;
    }
}