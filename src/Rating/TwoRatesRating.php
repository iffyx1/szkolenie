<?php

declare(strict_types=1);

namespace App\Rating;

use App\CreditRequestStatus;
use App\RatingInterface;

final class TwoRatesRating implements RatingInterface
{

    public function rateStatus(array $rates): string
    {
        foreach ($rates as $rate) {
            if ($rate->rate === 0) {
                return CreditRequestStatus::REJECTED;
            }
        }
        if (count($rates) === 2) {
            return CreditRequestStatus::ACCEPTED;
        }
        return CreditRequestStatus::IN_VERIFICATION;
    }
}