<?php

declare(strict_types=1);

namespace App\Rating;

use App\CreditRequestStatus;
use App\RatingInterface;

final class GraterPositiveRatesRating implements RatingInterface
{
    private int $rateAmount;

    public function __construct(int $rateAmount)
    {
        $this->rateAmount = $rateAmount;
    }

    public function rateStatus(array $rates): string
    {
        if (count(array_filter($rates, function ($r) {return $r->rate === 1;})) > $this->rateAmount / 2) {
            return CreditRequestStatus::ACCEPTED;
        }
        if (count(array_filter($rates, function ($r) {return $r->rate === 0;})) > $this->rateAmount / 2) {
            return CreditRequestStatus::REJECTED;
        }

        return CreditRequestStatus::IN_VERIFICATION;
    }
}