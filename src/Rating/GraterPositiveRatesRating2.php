<?php

declare(strict_types=1);

namespace App\Rating;

use App\CreditRequestStatus;
use App\RatingInterface;

final class GraterPositiveRatesRating implements RatingInterface
{
    private int $rateAmount;
    private int $minThreshold;

    public function __construct(int $rateAmount, int $minTreshold)
    {
        $this->rateAmount = $rateAmount;
        $this->minThreshold = $minTreshold;
    }

    public function rateStatus(array $rates): string
    {
        if (count(array_filter($rates, function ($r) {return $r->rate === 1;})) > $this->minThreshold) {
            return CreditRequestStatus::ACCEPTED;
        }
        if (count(array_filter($rates, function ($r) {return $r->rate === 0;})) > $this->minThreshold) {
            return CreditRequestStatus::REJECTED;
        }

        return CreditRequestStatus::IN_VERIFICATION;
    }
}