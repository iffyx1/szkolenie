<?php

declare(strict_types=1);

namespace App;

interface CreditRequestInterface
{
    public function createRequest();
    public function verify();
    public function updateRequest(float $price, string $description);
    public function giveRate(Rate $rate);
    public function copy();
}