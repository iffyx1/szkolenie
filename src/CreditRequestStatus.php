<?php

declare(strict_types=1);

namespace App;

final class CreditRequestStatus
{
    public const DRAFT = 'DRAFT';
    public const VERIFIED = 'VERIFIED';
    public const ACCEPTED = 'ACCEPTED';
    public const REJECTED = 'REJECTED';
    public const IN_VERIFICATION = 'IN_VERIFICATION';

}