<?php

declare(strict_types=1);

namespace App;

interface NumberGenerator
{
    public function generate(CreditRequestType $type): Number;
}