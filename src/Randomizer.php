<?php

declare(strict_types=1);

namespace App;

class Randomizer
{
    private int $min;
    private int $max;

    public function __construct(int $min, int $max)
    {
        $this->min = $min;
        $this->max = $max;
    }

    public function random(): int
    {
        return random_int(1000, 9999);
    }
}