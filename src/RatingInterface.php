<?php

declare(strict_types=1);

namespace App;

interface RatingInterface
{
    /**
     * @param Rate[] $rates
     *
     * @return string
     */
    public function rateStatus(array $rates): string;
}