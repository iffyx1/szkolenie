<?php

declare(strict_types=1);

namespace App;

final class CantVerifyException extends \Exception
{
    protected $message = 'Nie można zatwierdzić wniosku';
}