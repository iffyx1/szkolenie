<?php

declare(strict_types=1);

namespace App;

class SuffixNumberGenerator implements NumberGenerator
{

    private NumberGenerator $numberGenerator;
    private string $suffix;

    public function __construct(NumberGenerator $numberGenerator, string $suffix)
    {
        $this->numberGenerator = $numberGenerator;
        $this->suffix = $suffix;
    }

    public function generate(CreditRequestType $type): Number
    {
        return new Number($this->numberGenerator->generate($type)->getNumber().$this->suffix);
    }
}