<?php

declare(strict_types=1);

namespace App;

class PrefixNumberGenerator implements NumberGenerator
{
    private NumberGenerator $numberGenerator;
    private string $prefix;

    public function __construct(NumberGenerator $numberGenerator, string $prefix)
    {
        $this->numberGenerator = $numberGenerator;
        $this->prefix = $prefix;
    }

    public function generate(CreditRequestType $type): Number
    {
        return new Number($this->prefix.$this->numberGenerator->generate($type)->getNumber());
    }
}