<?php

declare(strict_types=1);

namespace App\NumberGenerator;

use App\CreditRequestType;
use App\Number;
use App\NumberGenerator;
use App\Randomizer;
use DateTimeZone;
use Lcobucci\Clock\Clock;
use Lcobucci\Clock\SystemClock;

final class StandardNumberGenerator implements NumberGenerator
{
    private Clock $clock;
    private Randomizer $randomizer;

    public function __construct(Clock $clock, Randomizer $randomizer)
    {
        $this->clock = $clock;
        $this->randomizer = $randomizer;
    }

    public function generate(CreditRequestType $type): Number
    {
        return new Number($type->name.'/'.$this->clock->now()->format('d/m/Y').'/'.$this->randomizer->random());
    }
}