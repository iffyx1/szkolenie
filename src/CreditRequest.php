<?php

declare(strict_types=1);

namespace App;

use App\Rating\TwoRatesRating;

final class CreditRequest implements CreditRequestInterface
{
    private CreditRequestType $type;
    private string $status;
    private int $id;
    private Number $number;
    /** @var Rate[] */
    private array $rate = [];
    private float $price;
    private string $description;
    private RatingInterface $rating;

    public function __construct(
        int $id,
        CreditRequestType $type,
        float $price,
        string $description,
        NumberGenerator $generator,
        RatingInterface $rating = null
    ) {
        $this->id = $id;
        $this->type = $type;
        $this->price = $price;
        $this->description = $description;
        $this->number = $generator->generate($type);
        $this->status = CreditRequestStatus::DRAFT;
        $this->rating = $rating ?? new TwoRatesRating();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return CreditRequestType
     */
    public function getType(): CreditRequestType
    {
        return $this->type;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return Number
     */
    public function getNumber(): Number
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return array
     */
    public function getRate(): array
    {
        return $this->rate;
    }

    private function accept() {}
    private function reject() {}

    public function createRequest()
    {
        // TODO: Implement createRequest() method.
    }

    public function verify()
    {
        if ($this->status !== CreditRequestStatus::DRAFT) {
            throw new CantVerifyException();
        }
        if (count($this->rate) >= 1) {
            throw new CantVerifyException();
        }

        $this->status = CreditRequestStatus::VERIFIED;
    }

    public function updateRequest(float $price, string $description)
    {
        // TODO: Implement updateRequest() method.
    }

    public function giveRate(Rate $rate)
    {
        if ($this->status !== CreditRequestStatus::VERIFIED) {
            throw new \Exception();
        }

        $this->rate[] = $rate;
        $this->status = $this->rating->rateStatus($this->rate);
    }

    public function copy()
    {
        // TODO: Implement copy() method.
    }
}