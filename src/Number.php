<?php

declare(strict_types=1);

namespace App;

final class Number
{
    private string $number;

    public function __construct(string $number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }
}