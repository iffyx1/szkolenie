<?php

declare(strict_types=1);

namespace App;

final class CreditRequestType
{
    public string $name;

    public function __construct(string $name = 'name')
    {
        $this->name = $name;
    }
}