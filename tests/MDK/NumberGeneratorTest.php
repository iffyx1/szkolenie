<?php

declare(strict_types=1);

namespace MDK;

use App\SuffixNumberGenerator;
use App\CreditRequestType;
use App\Number;
use App\NumberGenerator;
use App\NumberGenerator\StandardNumberGenerator;
use App\Randomizer;
use App\PrefixNumberGenerator;
use Lcobucci\Clock\FrozenClock;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

final class NumberGeneratorTest extends TestCase
{
    use ProphecyTrait;

    public function testGenerate()
    {
        $date = '01/01/2022';
        $type = 'type';

        $randomizer = $this->prophesize(Randomizer::class);
        $randomizer->random()->willReturn(6666);
        $randomizer = $randomizer->reveal();

        $generator = new StandardNumberGenerator(new FrozenClock(new \DateTimeImmutable($date)), $randomizer);
        $number = $generator->generate(new CreditRequestType($type));

        $this->assertEquals($type.'/'.$date.'/6666', $number->getNumber());
    }

    /**
     * @dataProvider createGenerator()
     */
    public function testTestNumberGenerator(NumberGenerator $numberGenerator, $result)
    {
        $number = $numberGenerator->generate(new CreditRequestType());

        $this->assertEquals($result, $number->getNumber());
    }

    public function createGenerator(): array
    {
        return [
            "test standard number generator"  =>
                [
                    new PrefixNumberGenerator(
                        $this->getNumberGenerator(),
                        "TEST::"
                    ),
                    'TEST::1/1/1',
                ],
            "audit standard number generator" =>
                [
                    new SuffixNumberGenerator(
                        $this->getNumberGenerator(),
                        "::AUDIT"
                    ),
                    '1/1/1::AUDIT',
                ],
            "combo standard number generator" => [
                new SuffixNumberGenerator(
                    new PrefixNumberGenerator(
                        $this->getNumberGenerator(),
                        "TEST::"
                    ),
                    "::AUDIT"
                ),
                "TEST::1/1/1::AUDIT"
            ],

        ];
    }

    private function getNumberGenerator(): NumberGenerator
    {
        $generator = $this->prophesize(NumberGenerator::class);
        $generator->generate(Argument::any())->willReturn(new Number('1/1/1'));

        return $generator->reveal();
    }
}