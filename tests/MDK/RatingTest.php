<?php

declare(strict_types=1);

namespace MDK;

use App\CreditRequestStatus;
use App\Rate;
use App\Rating\GraterPositiveRatesRating;
use App\Rating\TwoRatesRating;
use PHPUnit\Framework\TestCase;

final class RatingTest extends TestCase
{
    /**
     * @dataProvider createRatesToTwoRatesRating()
     */
    public function testTwoRatesRating(array $rates, string $expectedStatus)
    {
        $rating = new TwoRatesRating();
        $status = $rating->rateStatus($rates);
        $this->assertEquals($expectedStatus, $status);
    }

    public function createRatesToTwoRatesRating()
    {
        return [
            'two rates rating accepted' => [
                [
                    new Rate(1,1),
                    new Rate(1,2),
                ],
                CreditRequestStatus::ACCEPTED
            ],
            'two rates rating rejected' => [
                [
                    new Rate(0,1),
                ],
                CreditRequestStatus::REJECTED
            ],
            'second rate negative rejected' => [
                [
                    new Rate(1,1),
                    new Rate(0,2),
                ],
                CreditRequestStatus::REJECTED
            ],
            'one positive rate in verification' => [
                [
                    new Rate(1,1),
                ],
                CreditRequestStatus::IN_VERIFICATION
            ],
        ];
    }

    /**
     * @dataProvider createRatesToFiveRatesRating()
     */
    public function testFiveRatesRating(array $rates, string $expectedStatus)
    {
        $rating = new GraterPositiveRatesRating(4);
        $status = $rating->rateStatus($rates);
        $this->assertEquals($expectedStatus, $status);
    }

    public function createRatesToFiveRatesRating()
    {
        return [
            'five rating accepted' => [
                [
                    new Rate(1,1),
                    new Rate(1,2),
                    new Rate(1,3),
                ],
                CreditRequestStatus::ACCEPTED
            ],
            'five rating with one negative accepted' => [
                [
                    new Rate(1,1),
                    new Rate(0,2),
                    new Rate(1,3),
                    new Rate(1,4),
                ],
                CreditRequestStatus::ACCEPTED
            ],
            'five rating with two negative accepted' => [
                [
                    new Rate(1,1),
                    new Rate(0,2),
                    new Rate(1,3),
                    new Rate(0,4),
                    new Rate(1,5),
                ],
                CreditRequestStatus::ACCEPTED
            ],
            'five rating with three negative accepted' => [
                [
                    new Rate(0,1),
                    new Rate(0,2),
                    new Rate(1,3),
                    new Rate(1,4),
                    new Rate(0,5),
                ],
                CreditRequestStatus::REJECTED
            ],
            'five rating with two negative and two positive accepted' => [
                [
                    new Rate(0,1),
                    new Rate(0,2),
                    new Rate(1,3),
                    new Rate(1,4),
                ],
                CreditRequestStatus::IN_VERIFICATION
            ],
        ];
    }
}