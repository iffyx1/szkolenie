<?php

declare(strict_types=1);

namespace MDK;

use App\CantVerifyException;
use App\CreditRequest;
use App\CreditRequestStatus;
use App\CreditRequestType;
use App\Number;
use App\NumberGenerator;
use App\NumberGenerator\StandardNumberGenerator;
use App\Randomizer;
use App\Rate;
use DateTimeZone;
use Lcobucci\Clock\SystemClock;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

final class CRTest extends TestCase
{
    use ProphecyTrait;

    public function testDouble()
    {
        $generator = $this->prophesize(NumberGenerator::class);
        $generator->generate(Argument::type(CreditRequestType::class))->willReturn(new Number('aa'));
        $generator = $generator->reveal();

        $this->assertInstanceOf(NumberGenerator::class, $generator);
    }

    public function testWorkerId()
    {
        $id = 1;

        $request = new CreditRequest(
            $id, new CreditRequestType(), 2.0, 'opis', new StandardNumberGenerator(
            new SystemClock(new DateTimeZone('America/Sao_Paulo')),
            new Randomizer(1000, 9999)
        )
        );

        $this->assertEquals($id, $request->getId());
    }

    public function testType()
    {
        $type = new CreditRequestType();

        $request = new CreditRequest(
            1,
            $type,
            2.0,
            'opis',
            new StandardNumberGenerator(
                new SystemClock(new DateTimeZone('America/Sao_Paulo')),
                new Randomizer(1000, 9999)
            )
        );

        $this->assertEquals($type, $request->getType());
    }

    public function testPrice()
    {
        $price = 2.0;

        $request = new CreditRequest(
            1,
            new CreditRequestType(),
            $price,
            'opis',
            new StandardNumberGenerator(
                new SystemClock(new DateTimeZone('America/Sao_Paulo')),
                new Randomizer(1000, 9999)
            )
        );

        $this->assertEquals($price, $request->getPrice());
    }

    public function testDescription()
    {
        $description = 'opis';

        $request = new CreditRequest(
            1,
            new CreditRequestType(),
            2.0,
            $description,
            new StandardNumberGenerator(
                new SystemClock(new DateTimeZone('America/Sao_Paulo')),
                new Randomizer(1000, 9999)
            )
        );

        $this->assertEquals($description, $request->getDescription());
    }

    public function testStatus()
    {
        $request = $this->getCreditRequest(2.0);

        $this->assertEquals(CreditRequestStatus::DRAFT, $request->getStatus());
    }

    public function testNumber()
    {
        $request = $this->getCreditRequest(2.0);

        $this->assertNotEmpty($request->getNumber());
    }

    public function testNumber2()
    {
        $request = $this->getCreditRequest(2.0);

        $this->assertInstanceOf(Number::class, $request->getNumber());
    }

    public function testCanVerify()
    {
        $request = $this->getCreditRequest(2.0);
        $request->verify();

        $this->assertEquals(CreditRequestStatus::VERIFIED, $request->getStatus());
    }

    /**
     * @dataProvider requestExceptionData
     */
    public function testCantVerify(CreditRequest $request)
    {
        $this->expectException(CantVerifyException::class);
        $request->verify();
    }

    /**
     * @param $price
     *
     * @return CreditRequest
     */
    private function getCreditRequest($price): CreditRequest
    {
        return new CreditRequest(
            1,
            new CreditRequestType(),
            $price,
            'opis',
            new StandardNumberGenerator(
                new SystemClock(new DateTimeZone('America/Sao_Paulo')),
                new Randomizer(1000, 9999)
            ));
    }

    public function requestExceptionData(): array
    {
        return [
            "when verified" => [$this->createVerifiedRequest()],
            "when have rating" => [$this->createRequestWithRating()],
        ];
    }

    public function createVerifiedRequest(): CreditRequest
    {
        $request = $this->getCreditRequest(2.0);
        $request->verify();

        return $request;
    }

    public function createRequestWithRating(): CreditRequest
    {
        $request = $this->createVerifiedRequest();
        $request->giveRate(new Rate(1, 0));

        return $request;
    }


    public function testCanRate()
    {
        $request = $this->createVerifiedRequest();

        $request->giveRate(new Rate(1, 0));
        $this->assertCount(1, $request->getRate());
    }

//    /**
//     * @dataProvider rateExceptionData
//     */
//    public function testCantRate(CreditRequest $request)
//    {
//        $this->expectException(\Exception::class);
//        $request->giveRate(new Rate(1, 0));
//    }

//    public function rateExceptionData(): array
//    {
//        return [
//            "when in draft" => [$this->getCreditRequest(2.0)],
//            "when accepted" => [$this->createAcceptedRequest()],
//            "when rejected" => [$this->createRejectedRequest()]
//        ];
//    }

//    public function createAcceptedRequest(): CreditRequest
//    {
//        $request = $this->createVerifiedRequest();
//        $request->giveRate(new Rate(1, 1));
//        $request->giveRate(new Rate(1, 2));
//
//        return $request;
//    }
//
//    public function createRejectedRequest(): CreditRequest
//    {
//        $request = $this->createVerifiedRequest();
//        $request->giveRate(new Rate(0, 1));
//
//        return $request;
//    }
}